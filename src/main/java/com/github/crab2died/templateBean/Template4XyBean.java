package com.github.crab2died.templateBean;

public class Template4XyBean {
	/**
	 * 姓名 A 0
	 * 城市	 1
		门店代码	2
		门店类型	I 9-->8
		门店名称	N 14-->13
		门店地址	O 15-->14 
		门店附近的公交站台名称	P 16-->15
		门店附近的明显或著名建筑物以及方位	 Q 17-->16 
		门店的招牌名	R 18-->17
		店面区号		S 19-->18
		店面号码	T 20-->19
		暗访情景	H 8-->7
		在SA到达的情况是否需要真实申请（与原始清单分配一致）	V 22--21
		市场 H 8-->7
	 */
	private String name;
	private String city;
	private String storeCode;
	private String storeType;
	private String storeName;
	private String storeAddr;
	private String storeBus;
	private String storeLocation;
	private String storeSpecial;
	private String storeQuNum;
	private String storeTel;
	private String darkMeet;
	private String sainfo;
	private String market;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getStoreType() {
		return storeType;
	}
	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getStoreAddr() {
		return storeAddr;
	}
	public void setStoreAddr(String storeAddr) {
		this.storeAddr = storeAddr;
	}
	public String getStoreBus() {
		return storeBus;
	}
	public void setStoreBus(String storeBus) {
		this.storeBus = storeBus;
	}
	public String getStoreLocation() {
		return storeLocation;
	}
	public void setStoreLocation(String storeLocation) {
		this.storeLocation = storeLocation;
	}
	public String getStoreSpecial() {
		return storeSpecial;
	}
	public void setStoreSpecial(String storeSpecial) {
		this.storeSpecial = storeSpecial;
	}
	public String getStoreQuNum() {
		return storeQuNum;
	}
	public void setStoreQuNum(String storeQuNum) {
		this.storeQuNum = storeQuNum;
	}
	public String getStoreTel() {
		return storeTel;
	}
	public void setStoreTel(String storeTel) {
		this.storeTel = storeTel;
	}
	public String getDarkMeet() {
		return darkMeet;
	}
	public void setDarkMeet(String darkMeet) {
		this.darkMeet = darkMeet;
	}
	public String getSainfo() {
		return sainfo;
	}
	public void setSainfo(String sainfo) {
		this.sainfo = sainfo;
	}
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public Template4XyBean(String name, String city, String storeCode, String storeType, String storeName,
			String storeAddr, String storeBus, String storeLocation, String storeSpecial, String storeQuNum,
			String storeTel, String darkMeet, String sainfo, String market) {
		super();
		this.name = name;
		this.city = city;
		this.storeCode = storeCode;
		this.storeType = storeType;
		this.storeName = storeName;
		this.storeAddr = storeAddr;
		this.storeBus = storeBus;
		this.storeLocation = storeLocation;
		this.storeSpecial = storeSpecial;
		this.storeQuNum = storeQuNum;
		this.storeTel = storeTel;
		this.darkMeet = darkMeet;
		this.sainfo = sainfo;
		this.market = market;
	}
	@Override
	public String toString() {
		return "Template4XyBean [name=" + name + ", city=" + city + ", storeCode=" + storeCode + ", storeType="
				+ storeType + ", storeName=" + storeName + ", storeAddr=" + storeAddr + ", storeBus=" + storeBus
				+ ", storeLocation=" + storeLocation + ", storeSpecial=" + storeSpecial + ", storeQuNum=" + storeQuNum
				+ ", storeTel=" + storeTel + ", darkMeet=" + darkMeet + ", sainfo=" + sainfo + ", market=" + market
				+ "]";
	}
	
	
	
	
}
