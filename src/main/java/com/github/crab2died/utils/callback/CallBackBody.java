package com.github.crab2died.utils.callback;

public abstract class CallBackBody  {
	public void onSuccess(Object context) {
        System.out.println("onSuccess");
    }
 
	public void onFailure(Object context) {
        System.out.println("onFailure");
    }
 
   /* public abstract void execute(Object context) throws Exception;*/
    
    public abstract void execute(int[] x,int[] y,String[] info,String path) throws Exception;
}
