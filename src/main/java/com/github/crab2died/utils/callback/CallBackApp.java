package com.github.crab2died.utils.callback;

public class CallBackApp {
	 public static void main(String[] args) {
		 
	        System.out.println("准备开始执行异步任务...");
	 
	         Object context = "上下文信息";
	         int[] x=new int[] {1};
	         int[] y=new int[] {1,2};
	         String[] info=new String[] {"1","2"};
	         String path="path";
	 
	        new CallBackTask(new CallBackBody() {
	 
	            public void onSuccess(Object context) {
	                System.out.println("\n成功后的回调函数...");
	                System.out.println(context);
	            }
	 
	            public void onFailure(Object context) {
	                System.out.println("\n失败后的回调函数...");
	                System.out.println(context);
	            }

				@Override
				public void execute(int[] x, int[] y, String[] info,String path) throws Exception {
					// TODO Auto-generated method stub
					System.out.println(x);
					System.out.println(y);
					System.out.println(info);
					System.out.println(path);
				}
	        }).start(x,y,info,path);
	 
	        System.out.println("\n异步任务已经开始，请等待完成...");
	    }
}
