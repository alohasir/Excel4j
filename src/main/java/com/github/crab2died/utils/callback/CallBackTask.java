package com.github.crab2died.utils.callback;

public class CallBackTask {
	private CallBackBody body;
	 
    public CallBackTask(CallBackBody body) {
        this.body = body;
    }
 
    public void start(int[] x, int[] y, String[] info,String path) {
        final Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    body.execute(x,y,info,path);
                } catch (Exception e) {
                    e.printStackTrace();
                    body.onFailure("fail");
                }
                body.onSuccess("success");
            }
        });
        t.start();
    }
}
