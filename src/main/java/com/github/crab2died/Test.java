package com.github.crab2died;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.github.crab2died.templateBean.Template4XyBean;
import com.sun.org.apache.bcel.internal.generic.NEW;

public class Test {
	public static void main(String[] args) throws Exception {
		//风险类
		/**
		 *  姓名 A 0
		 *  是否为市场 H 8-7
		 *  城市	 1
			门店代码	2
			门店类型	I 9-->8
			门店名称	N 14-->13
			门店地址	O 15-->14 
			门店附近的公交站台名称	P 16-->15
			门店附近的明显或著名建筑物以及方位	 Q 17-->16 
			门店的招牌名	R 18-->17
			店面区号		S 19-->18
			店面号码	T 20-->19
			暗访情景	H 8-->7
			在SA到达的情况是否需要真实申请（与原始清单分配一致）	V 22--21
		 */
		/**
		 * 市场
		 *  城市		
			门店代码		
			门店类型		
			门店名称		
			门店地址		
			门店附近的公交站台名称		
			门店附近的明显或著名建筑物以及方位		
			门店的招牌名		
			店面区号		
			店面号码		
			暗访情景		
			在SA到达的情况是否需要真实申请（与原始清单分配一致）	
		 */
		//获取总数据
		String path="C:\\Users\\Administrator\\Desktop\\世冠\\";
		String path_sum="清单分配 ZJ.xlsx";
		String path_template_market="市场调查报告.xlsx";
		String path_template_fx="风险类暗访报告.xlsx";
		List<List<String>> list = ExcelUtils.getInstance().readExcel2List(path+path_sum);
		List<Template4XyBean> listBean = new ArrayList<>();
		for (int i = 1; i < list.size(); i++) {
			Template4XyBean xyBean = null;
			xyBean=new Template4XyBean(list.get(i).get(0),list.get(i).get(1), list.get(i).get(2), list.get(i).get(8), list.get(i).get(13), list.get(i).get(14), 
					list.get(i).get(15), list.get(i).get(16), list.get(i).get(17), list.get(i).get(18), list.get(i).get(19), list.get(i).get(7), list.get(i).get(21), list.get(i).get(7));
			listBean.add(xyBean);
		}
		
		//ExcelUtils.getInstance().exportObjects2Excel(listBean, "C:\\Users\\Administrator\\Desktop\\1.xlsx");
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
		String path_current_time = simpleDateFormat.format(new Date())+"\\";
		File file = null;
		String current_file_operate_path="";
		for(int i=0;i<listBean.size();i++) {
			//根据当前path+时间+姓名
			if("市场".equals(listBean.get(i).getMarket().trim())) {
				file = new File(path+path_current_time+listBean.get(i).getName()+"\\"+path_template_market);
				//校验文件路径
				File checkFile = new File(path+path_current_time+listBean.get(i).getName()+"\\");
				if(!checkFile.exists()) {
					checkFile.mkdirs();
				}
				//复制模版文件并改名
				current_file_operate_path=path+path_current_time+listBean.get(i).getName()+"\\"+listBean.get(i).getStoreCode()+"_市场.xlsx";
				copyFile(new File(path+path_template_market),file,current_file_operate_path);
			}else {
				file = new File(path+path_current_time+listBean.get(i).getName()+"\\"+path_template_fx);
				//校验文件路径
				File checkFile = new File(path+path_current_time+listBean.get(i).getName()+"\\");
				if(!checkFile.exists()) {
					checkFile.mkdirs();
				}
				//复制模版文件并改名
				current_file_operate_path=path+path_current_time+listBean.get(i).getName()+"\\"+listBean.get(i).getStoreCode()+"_风险.xlsx";
				copyFile(new File(path+path_template_fx),file,current_file_operate_path);
			}
			//更新指定位置数据
			int[] xArr=new int[] {1,2,3,4,5,6,7,8,9,10,11,12};
			int[] yArr=new int[] {3,3,3,3,3,3,3,3,3,3,3,3,3};
			String[] info =new String[] {
					  listBean.get(i).getCity()
					, listBean.get(i).getStoreCode()
					, listBean.get(i).getStoreType()
					, listBean.get(i).getStoreName()
					, listBean.get(i).getStoreAddr()
					, listBean.get(i).getStoreBus()
					, listBean.get(i).getStoreLocation()
					, listBean.get(i).getStoreSpecial()
					, listBean.get(i).getStoreQuNum()
					, listBean.get(i).getStoreTel()
					, listBean.get(i).getDarkMeet()
					, listBean.get(i).getSainfo()
					};
			updateLocationCell(current_file_operate_path, xArr, yArr, info);//城市
		}
	}
	
	public static void updateLocationCell(String filePath,int[] x,int[] y,String[] value) {
		try {
			// 创建Excel的工作书册 Workbook,对应到一个excel文档
			System.out.println(filePath);
			XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(filePath));
			XSSFSheet sheet=wb.getSheetAt(0);
			for (int i = 0; i < value.length; i++) {
				XSSFRow row=sheet.getRow(x[i]-1);
				XSSFCell cell=row.getCell(y[i]);
				cell.setCellValue(value[i]);
			}
			FileOutputStream os;
			os = new FileOutputStream(filePath);
			wb.write(os);
			wb.close();
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
     * 复制文件
     * @param fromFile
     * @param toFile
     * <br/>
     * 2016年12月19日  下午3:31:50
     * @throws IOException 
     */
    public static void copyFile(File fromFile,File toFile,String toFilePath) throws IOException{
    	toFilePath=toFilePath.replace("\\\\", "/");
    	toFile= new File(toFilePath);
        FileInputStream ins = new FileInputStream(fromFile);
        FileOutputStream out = new FileOutputStream(toFile);
        byte[] b = new byte[4096];
        int n=0;
        while((n=ins.read(b))!=-1){
            out.write(b, 0, n);
        }
        
        ins.close();
        out.close();
    }
}
